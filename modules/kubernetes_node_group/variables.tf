variable "kubernetes_cluster_id" {}
variable "group_name" {}
variable "zone" {}
variable "ver" {
  default = "1.18"
}
variable "taints" {
  default = []
}
variable "labels" {
  default = {}
}
variable "platform" {
  default = "standard-v2"
}
variable "yandex_vpc_subnet" {}
variable "security_groupd" {
  default = []
}
variable "core" {
  default = "20"
}
variable "memory" {
  default = "2"
}
variable "cpu" {
  default = "2"
}
variable "disk_type" {
  default = "network-hdd"
}
variable "disk_size" {
  default = "64"
}
variable "node_init" {
  default = "1"
}
variable "node_min" {
  default = "1"
}
variable "node_max" {
  default = "2"
}